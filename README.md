# iOSTestTemplate

[![CI Status](https://img.shields.io/travis/Rajesh/iOSTestTemplate.svg?style=flat)](https://travis-ci.org/Rajesh/iOSTestTemplate)
[![Version](https://img.shields.io/cocoapods/v/iOSTestTemplate.svg?style=flat)](https://cocoapods.org/pods/iOSTestTemplate)
[![License](https://img.shields.io/cocoapods/l/iOSTestTemplate.svg?style=flat)](https://cocoapods.org/pods/iOSTestTemplate)
[![Platform](https://img.shields.io/cocoapods/p/iOSTestTemplate.svg?style=flat)](https://cocoapods.org/pods/iOSTestTemplate)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

iOSTestTemplate is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'iOSTestTemplate'
```

## Author

Rajesh, rajesht1989@gmail.com

## License

iOSTestTemplate is available under the MIT license. See the LICENSE file for more info.
