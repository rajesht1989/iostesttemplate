

public class ReplaceMe {
    public static func add(a: Int, b: Int, c: Int) -> Int {
        #if CUSTOM
        return a + b + c
        #else
        return a + b
        #endif
    }
}
